Pour démarrer le serveur node :

`node index.js`

Pour mettre à jour les dépendances :

- bower : `bower update`
- npm : `npm update`

