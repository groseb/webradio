var express = require('express');
var request = require('request');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var jsdom = require("jsdom").jsdom;
var SSH = require('simple-ssh');

var nbVotes = 0;
var nbListeners = 0;
var lastSong = '';
var currentSong = '';

var options = {
  url: "http://192.168.1.199:8000/index.html",
  headers: {
    'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Encoding':'gzip, deflate, sdch',
    'Accept-Language':'fr-FR,fr;q=0.8,en-US;q=0.6,en;q=0.4',
    'Cache-Control':'no-cache',
    'Connection':'keep-alive',
    'Host':'192.168.1.105:8000',
    'Pragma':'no-cache',
    'Referer':'http://192.168.1.199:8000/played.html',
    'Upgrade-Insecure-Requests':'1',
    'User-Agent':'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36'
  }
};

setInterval(function() {
  function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
      var document = jsdom(body, {});
      var window = document.defaultView;
      var $ = require('jquery')(window);
      currentSong = $("body>font>table:nth-child(4)>tbody>tr:nth-child(12)>td:nth-child(2)>font>b").text();
      if(lastSong !== currentSong) {
        lastSong = currentSong;
        nbVotes = 0;
        console.log('new song');
        io.emit('song', currentSong);
        io.emit('vote ended');
        io.emit('votes', nbVotes, nbListeners);
      }
      var listeners = $("body>font>table:nth-child(4)>tbody>tr:nth-child(2)>td:nth-child(2)>font>b>b").text();
      result = listeners.match(/\d* of \d* listeners \((\d*) unique\)/);
      if(result !== null) {
        nbListeners = result[1];
      } else {
        nbListeners = 1;
      }
    } else {
      console.log(error);
    }
  }
  request(options, callback);
}, 1500);

app.use(express.static('static'));

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function (socket) {
  io.emit('song', currentSong);
  io.emit('votes', nbVotes, nbListeners);
  socket.on('vote', function () {
    nbVotes++;
    if(nbVotes >= (nbListeners - nbListeners/2)) {
      nbVotes = 0;
      console.log('changing song');
      var ssh = new SSH({
        host: '192.168.1.199',
        user: 'webradio',
        pass: 'pwet'
      });
      ssh.exec('cd /home/webradio && ./restartsc_trans.sh && exit 0', {
        out: function(stdout) {
          io.emit('vote ended');
          console.log(stdout);
        }
      }).start();
    }
    io.emit('votes', nbVotes, nbListeners);
  });
  socket.on('update votes', function () {
    io.emit('votes', nbVotes, nbListeners);
  });
});

http.listen(3000, function () {
  console.log('listening on *:3000');
});
